// INFO-F201 - Projet
// Noëmie Muller
// 10/12/2018.
// 000458865
// Partie Serveur

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>

#define MYPORT 5555
#define BUFFSIZE 1024
#define BACKLOG 10

int main(int argc, char *argv[])
{
    int sockfd, new_fd; //écoute sur sockfd, nouvelle connection sur new_fd
    struct sockaddr_in my_addr; //informations concernant la machine du serveur
    struct sockaddr_in their_addr; //informations concernant la machine client
    unsigned int sin_size;
    char buffer[BUFFSIZE];
    int yes=1;
    
    //check bon nombre d'arguments
    if (argc != 2){
        fprintf(stderr, "Donner le chemin vers poolv2 \n");
        return EXIT_FAILURE;
    }
    
    //création du socket d'écoute
    if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Serveur: socket");
    }
    
    //permet de ne pas provoquer d'erreur lors de deux appels successifs à bind()
    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
        perror("Serveur: setsockopt");
        return EXIT_FAILURE;
    }
    
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MYPORT);
    my_addr.sin_addr.s_addr=INADDR_ANY;
    memset(&(my_addr.sin_zero), '\0',8);
    
    if (bind(sockfd,(struct sockaddr *)&my_addr,sizeof(struct sockaddr)) == -1){
        perror("Serveur: bind");
        return EXIT_FAILURE;
    }
    
    if (listen(sockfd, BACKLOG) == -1){
        perror("Serveur: listen");
        return EXIT_FAILURE;
    }
    
    sin_size=sizeof(struct sockaddr_in);
    
    while(1){
        if ((new_fd = accept(sockfd,(struct sockaddr *)&their_addr,&sin_size)) == -1){
            perror("Serveur: accept");
        }
        printf("Serveur: connection reçue du client %s\n",inet_ntoa(their_addr.sin_addr));
        if (fork()==0){
            close(sockfd);
            int username_size,filename_size;
            
            //réception de la taille de username
            if((recv(new_fd,&username_size,sizeof(int),0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            username_size=ntohl(username_size);
            
            //réception de username
            char username[username_size+1];
            if ((recv(new_fd,&username,username_size,0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            username[username_size]='\0';
            
            //réception de la date de prise de la photo
            char year[5],month[3],day[3];
            if ((recv(new_fd,&year,4,0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            if ((recv(new_fd,&month,2,0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            if ((recv(new_fd,day,2,0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            year[4] = '\0';
            month[2] = '\0';
            day[2] = '\0';
            
            //réception de la taille du string nom de fichier
            if((recv(new_fd,&filename_size,sizeof(int),0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            filename_size=ntohl(filename_size);
            
            //réception de username
            char filename[filename_size+1];
            if ((recv(new_fd,&filename,filename_size,0))==-1){
                perror("Serveur: recv");
                return EXIT_FAILURE;
            }
            filename[filename_size]='\0';
            
            FILE* image;
            char temp[100]="/tmp";
            strcat(temp, filename);
            image=fopen(filename,"w");
            
            
            long int nb = BUFFSIZE;
            int count = 0;
            while(nb==BUFFSIZE){
                nb=recv(sockfd,&buffer,BUFFSIZE,0);
                count+=nb;
                fwrite(buffer,1,nb,image);
                memset(buffer,'\0',BUFFSIZE);
            }
            
            fclose(image);
            char *path;
            path=argv[1];
            if (path[strlen(path)-1] != '/'){
                strcat(path, "/");
            }
            strcat(path, username);
            mkdir(path,700);
            strcat(path, "/");
            strcat(path, year);
            mkdir(path,700);
            strcat(path, "/");
            strcat(path, month);
            mkdir(path,700);
            strcat(path, "/");
            strcat(path, day);
            mkdir(path,700);
            strcat(path, "/");
            strcat(path, filename);
            
            FILE* fichier_temporaire;
            FILE* nouveau_fichier;
            fichier_temporaire=fopen(temp,"r");
            nouveau_fichier=fopen(path,"w");
            
            nb=BUFFSIZE;
            while (nb==BUFFSIZE){
                nb=fread(buffer,sizeof(char),BUFFSIZE,fichier_temporaire);
                fwrite(buffer,1,nb,nouveau_fichier);
                memset(buffer,'\0',BUFFSIZE);
            }
            fclose(fichier_temporaire);
            fclose(nouveau_fichier);
            remove(temp);
        }
    close(new_fd);
    return EXIT_SUCCESS;
    }
}

