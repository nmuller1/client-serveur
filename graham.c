// INFO-F201 - Projet
// Noëmie Muller
// 10/12/2018.
// 000458865
// Partie Client

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#define BUFFSIZE 1024

size_t envoi(int sockfd, const void *buf, long int size){
    size_t ret;
    if ((ret=send(sockfd,buf,size,0)) == -1){
        perror("Client: send");
        return EXIT_FAILURE;
    }
    return ret;
}

int main(int argc, char *argv[]){
    
    //déclaration des variables
    int sockfd ;
    struct sockaddr_in their_addr;
    FILE* image;
    char buff[BUFFSIZE];
    char *username,*path_to_image;
    char *year, *month, *day;
    char *pos,*filename; //pos sert au découpage du path pour extraire le nom du fichier
    long int l;
    
    //check nombre d'arguments ok
    if (argc != 7){
        fprintf(stderr, "Veuillez entrer les paramètres suivants : ip_server nom_utilisateur_dans_le_pool /chemin/vers/la/photo année mois jour");
        return EXIT_FAILURE;
    }
    
    //dispatch des arguments dans les variables
    username=argv[2];
    path_to_image=argv[3];
    year=argv[4];
    month=argv[5];
    day=argv[6];

    //extraction du nom du fichier
    if((pos=strrchr(path_to_image,'/'))){
        l = strlen(pos);
        filename=malloc(l+1);
        strcpy(filename,pos);
    }

    //création du socket client
    if ((sockfd = socket(PF_INET,SOCK_STREAM,0)) == -1){
        perror("Client : socket");
        return EXIT_FAILURE;
    }
    
    //initialisation de la structure de données du socket
    their_addr.sin_family = AF_INET;
    their_addr.sin_port = 0; //c'est le système qui choisit un port libre au hasard
    their_addr.sin_addr.s_addr = inet_addr(argv[1]);
    memset(&(their_addr.sin_zero), '\0',8);
    
    //association du socket avec le numéro de port sur la machine locale
    if (bind(sockfd,(struct sockaddr *)&their_addr,sizeof(struct sockaddr)) == -1){
        perror("Serveur: bind");
        return EXIT_FAILURE;
    }
    
    //connection au serveur
    if(connect(sockfd, (struct sockaddr *)&their_addr, sizeof(struct sockaddr))== -1){
        perror("Client: connect");
        return EXIT_FAILURE;
    }
    
    //envoi des informations concernant l'image
    long int username_size = strlen(username);
    envoi(sockfd,username_size,sizeof(int));
    envoi(sockfd,username,username_size);
    envoi(sockfd,year,4);
    envoi(sockfd,month,2);
    envoi(sockfd,day,2);
    long int filename_size = strlen(filename);
    envoi(sockfd,filename_size,sizeof(int));
    envoi(sockfd,filename,filename_size);
    
    //ouverture du fichier image à envoyer au serveur
    if ((image=fopen(path_to_image,"r")) == NULL){
        perror("Client: fopen");
        return EXIT_FAILURE;
    }
    
    //envoi de l'image par paquets de BUFFISZE octets
    long int nb =  BUFFSIZE; //nombre de caractères envoyés
    while(nb==BUFFSIZE){
        nb = fread(buff,sizeof(char),BUFFSIZE,image);
        envoi(sockfd,&buff,nb);
        memset(buff,'\0',BUFFSIZE);
    }
    
    /*code retour du serveur
    int *code_retour;
    recv(sockfd,&code_retour,sizeof(int),0);
    code_retour=ntohl(*code_retour);
    
    */
    fclose(image);
    close(sockfd);
}
